var gulp     = require('gulp'),
    less     = require('gulp-less'),
    rename   = require('gulp-rename'),
    watch    = require('gulp-watch'),
    concat   = require('gulp-concat'),
    annotate = require('gulp-ng-annotate'),
    uglify   = require('gulp-uglify');

gulp.task('less', function() {
    return gulp.src('src/resources/assets/less/master.less')
        .pipe(less())
        .pipe(rename('panda.css'))
        .pipe(gulp.dest('../public/css'));
});

gulp.task('js', function() {
    var files = [
        'node_modules/jquery/dist/jquery.js',
        //'node_modules/angular/angular.js',
        //'node_modules/angular-smart-table/dist/smart-table.js',
        //'node_modules/angular-ui-tree/dist/angular-ui-tree.js',
        'node_modules/bootstrap/dist/js/bootstrap.js',
        'node_modules/alertify.js/dist/js/alertify.js',
        'node_modules/dropzone/dist/dropzone.js',
        'node_modules/fancybox/dist/js/jquery.fancybox.js',
        'src/resources/assets/js/*',
        //'src/resources/assets/angular/**/*.js'
    ];
    return gulp.src(files)
        .pipe(concat('panda.js'))
        .pipe(annotate())
        .pipe(uglify())
        .pipe(rename('panda.min.js'))
        .pipe(gulp.dest('../public/js'));
});

gulp.task('fonts', function() {
    return gulp.src([
        'node_modules/font-awesome/fonts/*'
    ])
    .pipe(gulp.dest('../public/fonts'));
});

gulp.task('fancybox', function() {
    return gulp.src('node_modules/fancybox/dist/img/*')
        .pipe(gulp.dest('../public/components/fancybox/source'));
});

gulp.task('ckeditor', function() {
    gulp.src([
        'node_modules/ckeditor/ckeditor.js',
        'node_modules/ckeditor/styles.js',
        'node_modules/ckeditor/contents.css'
    ])
    .pipe(gulp.dest('../public/components/ckeditor'));
    gulp.src([
        'node_modules/ckeditor/lang/en.js'
    ])
    .pipe(gulp.dest('../public/components/ckeditor/lang'));
    var plugins = [
        'clipboard',
        'image',
        'image2',
        'justify',
        'lineutils',
        'link',
        'magicline',
        'panelbutton',
        'pastefromword',
        'showblocks',
        'specialchar',
        'table',
        'widget'
    ];
    for(var i = 0; i < plugins.length; i++) {
        gulp.src(['node_modules/ckeditor/plugins/' + plugins[i] + '/**/*'])
            .pipe(gulp.dest('../public/components/ckeditor/plugins/' + plugins[i]));
    }
});

gulp.task('watch', function() {
    gulp.watch('src/resources/assets/less/*.less', ['less']);
    //gulp.watch('src/resources/assets/angular/**/*.js', ['js']);
    gulp.watch('src/resources/assets/js/*.js', ['js']);
});

gulp.task('all', [
    'less',
    'js',
    'fancybox',
    'ckeditor',
    'fonts',
    'watch'
]);

gulp.task('default', [
    'less',
    'js',
    'watch'
]);