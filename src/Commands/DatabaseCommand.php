<?php

namespace Panda\Commands;

use Illuminate\Console\Command;

class DatabaseCommand extends Command
{
    protected $name = 'panda:database';

    protected $description = '...';

    public function fire()
    {
        //$this->call('migrate');
        $this->call('db:seed --class=\Panda\Database\Seeders\InstallSeeder');
    }
}