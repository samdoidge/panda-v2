<?php

namespace Panda\Commands;

use Illuminate\Console\Command;
use Panda\Models\User;

class InstallCommand extends Command
{
    protected $name = 'panda:install';
    
    protected $description = '...';
    
    public function fire()
    {
        $this->createUser();
    }

    private function createUser()
    {
        $this->info('Creating user...');
        
        $data = [
            'email' => 'ben@email.com',
            'password' => bcrypt('password')
        ];
        
        User::create($data);
    }
}