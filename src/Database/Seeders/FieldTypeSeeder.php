<?php

namespace Panda\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class FieldTypeSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Block Field',
                'class' => '\Panda\Fields\BlockField',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ],
            [
                'id' => 2,
                'name' => 'Image Field',
                'class' => '\Panda\Fields\ImageField',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ],
            [
                'id' => 3,
                'name' => 'Select Field',
                'class' => '\Panda\Fields\SelectField',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ],
            [
                'id' => 4,
                'name' => 'Text Field',
                'class' => '\Panda\Fields\TextField',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ],
            [
                'id' => 5,
                'name' => 'Wysiwyg Field',
                'class' => '\Panda\Fields\WysiwygField',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ]
        ];

        DB::table('field_types')->insert($data);
    }
}