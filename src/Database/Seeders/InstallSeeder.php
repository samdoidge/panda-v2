<?php

namespace Panda\Database\Seeders;

use Illuminate\Database\Seeder;

class InstallSeeder extends Seeder
{
    public function run()
    {
        $this->call(FieldTypeSeeder::class);
        $this->call(TemplateTypeSeeder::class);
    }
}