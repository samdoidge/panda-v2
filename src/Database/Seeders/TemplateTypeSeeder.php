<?php

namespace Panda\Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TemplateTypeSeeder extends Seeder
{
    public function run()
    {
        $data = [
            [
                'id' => 1,
                'name' => 'Page',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ],
            [
                'id' => 2,
                'name' => 'Block',
                'updated_at' => 'NOW()',
                'created_at' => 'NOW()'
            ]
        ];

        DB::table('template_types')->insert($data);
    }
}