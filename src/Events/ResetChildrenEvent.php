<?php

namespace Panda\Events;

use Illuminate\Events\Dispatcher;
use Panda\Models\Page;

class ResetChildrenEvent
{
    public function resetChildrenUri(Page $model)
    {
        foreach($model->children as $child)
        {
            if(is_null($model->uri)) {
                $child->uri = null;
            } else {
                $child->uri = '';
            }

            $child->save();
            $this->resetChildrenUri($child);
        }
    }

    public function subscribe(Dispatcher $events)
    {
        $events->listen('resetChildrenUri', 'Panda\Events\ResetChildrenEvent@resetChildrenUri');
    }
}