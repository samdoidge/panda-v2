<?php

namespace Panda\Facades;

use Illuminate\Support\Facades\Facade;

class PageFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'Panda\Repositories\PageRepository';
    }
}