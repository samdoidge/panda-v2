<?php

namespace Panda\Fields;

use Illuminate\View\View;

class BaseField
{
    /**
     * The view object associated with the field.
     *
     * @var View
     */
    protected $view;

    /**
     * BaseField constructor.
     * 
     * @param View $view The view object for the field.
     */
    public function __construct(View $view)
    {
        $this->view = $view;
    }

    /**
     * Return the HTML output of the view.
     *
     * @return string
     */
    public function render()
    {
        return $this->view->render();
    }
    
    public static function instance($field, $value)
    {
        return $field->view->render();
    }
}