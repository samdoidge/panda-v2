<?php

namespace Panda\Fields;

use Panda\Models\Field;
use Panda\Models\Template;
use Panda\Interfaces\FieldInterface;

class BlockField extends BaseField implements FieldInterface
{
    public function __construct(Field $field, $value)
    {
        $data = [];

        $multiple = false;

        $options = $field->options->lists('value', 'option');

        if(isset($options['template_id'])) {
            $template = Template::find($options['template_id']);
            $blocks = $template->blocks;

            if(!is_null($blocks)) {
                $data = $blocks->lists('title', 'id');
            }
        }

        if(isset($options['multiple']) && $options['multiple']) {
            $multiple = true;
            $value = json_decode($value);
        }

        $view = view('panda::fields.select-field',
            compact('value', 'data', 'field', 'multiple'));

        parent::__construct($view);
    }
}