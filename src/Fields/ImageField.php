<?php

namespace Panda\Fields;

use Panda\Interfaces\FieldInterface;
use Panda\Models\Field;

class ImageField extends BaseField implements FieldInterface
{
    public function __construct(Field $field, $value)
    {
        $images = [];

        if($value) {
            $images = json_decode($value);
        }

        $view = view('panda::fields.image-field',
            compact('value', 'field', 'images'));
        
        parent::__construct($view);
    }
}
