<?php

namespace Panda\Fields;

use Panda\Interfaces\FieldInterface;
use Panda\Models\Field;
use Panda\Models\Template;

class SelectField extends BaseField implements FieldInterface
{
    public function __construct(Field $field, $value)
    {
    	$data = [];

    	$options = $field->options->lists('value', 'option');

    	if(isset($options['template_id'])) {
    		$template = Template::find($options['template_id']);
    		$pages = $template->pages;

    		if(!is_null($pages)) {
    			$data = $pages->lists('title', 'id');
    		}
    	} else if(isset($options['json'])) {
    		$data = json_decode($options['json']);
    	}

        $view = view('panda::fields.select-field',
            compact('value', 'data', 'field'));
        
        parent::__construct($view);
    }
}
