<?php

namespace Panda\Fields;

use Panda\Interfaces\FieldInterface;
use Panda\Models\Field;

class TextField extends BaseField implements FieldInterface
{
    public function __construct(Field $field, $value)
    {
        $view = view('panda::fields.text-field',
            compact('value', 'field'));
        
        parent::__construct($view);
    }
}