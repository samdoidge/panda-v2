<?php

namespace Panda\Fields;

use Panda\Interfaces\FieldInterface;
use Panda\Models\Field;

class WysiwygField extends BaseField implements FieldInterface
{
    public function __construct(Field $field, $value)
    {
        $view = view('panda::fields.wysiwyg-field',
            compact('field', 'value'));
        
        parent::__construct($view);
    }
}