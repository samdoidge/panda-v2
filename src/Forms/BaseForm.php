<?php

namespace Panda\Forms;

use Panda\Models\Block;
use Panda\Models\Page;
use Panda\Models\Template;

class BaseForm
{
    /**
     * The template associated with the form.
     *
     * @var Template
     */
    protected $template;

    /**
     * BaseForm constructor.
     *
     * @param Template $template The main template associated with our form.
     */
    public function __construct(Template $template)
    {
        $this->template = $template;
    }

    /**
     * Returns the fields associated with the protected template.
     *
     * @param Block|Page $model The page or block to take values from.
     *
     * @return string
     */
    public function render($model = null)
    {
        // Initialise the HTML variable.
        $html = '';

        // Get all fields associated with the given template.
        $fields = $this->template->fields->sortBy('order');

        // Get all values associated with the given model.
        $values = $model ? $model->values->lists('value', 'field_id') : [];

        // Loop through all the fields so we can assign the correct values.
        foreach ($fields as $field) {

            // Check to see if we have a value for the field.
            $value = isset($values[$field->id]) ? $values[$field->id] : null;

            // Create a new instance of the fields type class.
            $class = new $field->type->class($field, $value);

            // Append the field to the HTML string.
            $html .= $class->render();
        }
        
        return $html;
    }
}
