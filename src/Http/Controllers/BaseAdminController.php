<?php

namespace Panda\Http\Controllers;

use JavaScript;
use Illuminate\Routing\Controller;

class BaseAdminController extends Controller
{
    protected $repository;

    public function __construct($repository = null)
    {
        $this->middleware('web');
        $this->repository = $repository;
    }

    public function index()
    {
        $module = $this->repository->getTable();
        $models = $this->repository->all([], true);

        return view('panda::pages.global.index')
            ->with(compact('module', 'models'));
    }

    protected function redirect($request, $model)
    {
        $redirectUrl = $request->get('exit') ? $model->indexUrl() : $model->editUrl();

        return redirect($redirectUrl);
    }
}