<?php

namespace Panda\Http\Controllers;

use JavaScript;
use Illuminate\Routing\Controller;

class BaseApiController extends Controller
{
    protected $repository;

    public function __construct($repository = null)
    {
        $this->middleware('admin');
        $this->repository = $repository;
    }
}
