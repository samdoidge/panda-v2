<?php

namespace Panda\Http\Controllers;

use Illuminate\Routing\Controller;

class BasePublicController extends Controller
{
    protected $repository;
    protected $request;

    public function __construct($repository = null)
    {
        $this->middleware('web');
        $this->repository = $repository;
        $this->request = request();
    }
}