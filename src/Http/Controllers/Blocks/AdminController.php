<?php

namespace Panda\Http\Controllers\Blocks;

use Illuminate\Http\Request;
use Panda\Models\Block;
use Panda\Models\Field;
use Panda\Models\Template;
use Panda\Models\TemplateType;
use Panda\Repositories\BlockRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
	public function __construct(BlockRepository $repository)
	{
		parent::__construct($repository);
	}

	public function create()
    {
        $model = $this->repository->getModel();

        $templates = Template::where('template_type_id', TemplateType::BLOCK)->lists('name', 'id');

        return view('panda::pages.global.create',
            compact('model', 'templates'));
    }

    public function edit(Block $model)
    {
        $templates = Template::where('template_type_id', TemplateType::BLOCK)->lists('name', 'id');

        return view('panda::pages.global.edit')
            ->with(compact('model', 'templates'));
    }

    public function store(Request $request)
    {
        $block = Block::create($request->all());

        if($block->save()) {
            if($request->has('fields')) {
                $this->saveFields($block, $request->get('fields'));
            }
        }

        return $this->redirect($request, $block);
    }

    public function update(Request $request, Block $model)
    {
        $model->update($request->all());

        if($request->has('fields')) {
            $this->saveFields($model, $request->get('fields'));
        }

        return $this->redirect($request, $model);
    }

    private function saveFields(Block $block, array $values)
    {
        foreach($values as $id => $value) {

            if(is_array($value)) {
                $value = json_encode($value);
            }

            $block->values()->updateOrCreate(['field_id' => $id],
                [
                    'value' => $value,
                    'field_id' => $id
                ]);
        }
    }
}