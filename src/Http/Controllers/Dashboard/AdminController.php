<?php

namespace Panda\Http\Controllers\Dashboard;

use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function index()
    {
        return view('panda::pages.dashboard.index');
    }
}