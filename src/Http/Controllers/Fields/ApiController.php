<?php

namespace Panda\Http\Controllers\Fields;

use Panda\Models\Block;
use Panda\Models\Page;
use Panda\Forms\BaseForm;
use Panda\Models\Template;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Response;
use Panda\Models\TemplateType;

class ApiController extends Controller
{
	public function index(Template $template, $model = null)
    {
        if($model) {
            switch($template->type->id) {
                case TemplateType::PAGE:
                    $model = Page::find($model);
                    break;
                case TemplateType::BLOCK;
                    $model = Block::find($model);
                    break;
            }
        }
        
        $form = new BaseForm($template);

        return Response::make($form->render($model), 200);
    }
}