<?php

namespace Panda\Http\Controllers\Files;

use Panda\Repositories\FileRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(FileRepository $repository)
    {
        parent::__construct($repository);
    }
}