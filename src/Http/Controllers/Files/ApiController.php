<?php

namespace Panda\Http\Controllers\Files;

use Panda\Models\File;
use Illuminate\Routing\Controller;
use Panda\Http\Requests\Files\FormRequest;

class ApiController extends Controller
{
    public function index()
    {
        $models = File::all();

        return response()
            ->json($models, 200);
    }

    public function store(FormRequest $request)
    {
        $model = File::create($request->all());
        $error = $model ? false : true;

        return response()->json([
            'error' => $error,
            'model' => $model
        ], 200);
    }

    public function destroy(File $file)
    {
        $deleted = $file->delete();

        return response()->json([
            'error' => !$deleted
        ]);
    }
}