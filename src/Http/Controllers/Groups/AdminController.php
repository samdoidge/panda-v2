<?php

namespace Panda\Http\Controllers\Groups;

use Panda\Models\Group;
use Panda\Repositories\GroupRepository;
use Panda\Http\Requests\Groups\FormRequest;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(GroupRepository $repository)
    {
        parent::__construct($repository);
    }

    public function create()
    {
        $model = $this->repository->getModel();

        return view('panda::pages.global.create')
            ->with(compact('model'));
    }

    public function edit(Group $model)
    {
        return view('panda::pages.global.edit')
            ->with(compact('model'));
    }

    public function store(FormRequest $request)
    {
        $model = $this->repository->create($request->all());

        return $this->redirect($request, $model);
    }

    public function update(Group $model, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $model);
    }
}
