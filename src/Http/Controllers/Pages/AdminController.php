<?php

namespace Panda\Http\Controllers\Pages;

use Illuminate\Foundation\Validation\ValidatesRequests;
use Panda\Models\Page;
use Illuminate\Http\Request;
use Panda\Models\Template;
use Panda\Models\TemplateType;
use Panda\Repositories\PageRepository;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    use ValidatesRequests;

    public function __construct(PageRepository $repository)
    {
        parent::__construct($repository);
    }

    public function index()
    {
        $module = $this->repository->getTable();
        $models = $this->repository->all([], true)
            ->nest()
            ->listsFlattened();

        return view('panda::pages.global.index')
            ->with(compact('module', 'models'));
    }

    public function create()
    {
        $model = $this->repository->getModel();

        $parents = $this->repository->all([], true)
            ->nest()
            ->listsFlattened();

        $templates = Template::where('template_type_id', TemplateType::PAGE)->lists('name', 'id');

        return view('panda::pages.global.create',
            compact('model', 'parents', 'templates'));
    }

    public function edit(Page $page)
    {
        $parents = $this->repository->allBut('id', $page->id, [], true)
            ->nest()
            ->listsFlattened();

        $templates = Template::where('template_type_id', TemplateType::PAGE)->lists('name', 'id');
        
        return view('panda::pages.global.edit')
            ->with([
                'model' => $page,
                'parents' => $parents,
                'templates' => $templates
            ]);
    }

    public function store(Request $request)
    {
        $template = Template::find($request->get('template_id'));
        
        $rules = $attributes = [];

        foreach($template->fields as $field) {
            if($field->validation) {
                $attribute = 'fields.'.$field->id;
                $rules[$attribute] = $field->validation;
                $attributes[$attribute] = strtolower($field->label);
            }
        }

        $this->validate($request, $rules, [], $attributes);

        $data = $request->all();

        if(isset($data['parent_id']) && !$data['parent_id']) {
            unset($data['parent_id']);
        }

        $page = Page::create($data);

        if($page->save()) {

            if(isset($data['fields'])) {
                $this->saveFields($page, $data['fields']);
            }
        }

        $this->repository->saveCache($page);

        return $this->redirect($request, $page);
    }

    public function update(Request $request, Page $page)
    {
        $data = $request->all();

        if(isset($data['parent_id']) && !$data['parent_id']) {
            unset($data['parent_id']);
        }

        $page->update($data);

        if(isset($data['fields'])) {
            $this->saveFields($page, $data['fields']);
        }

        $this->repository->saveCache($page);

        return $this->redirect($request, $page);
    }

    private function saveFields(Page $page, array $values)
    {
        foreach($values as $id => $value) {

            if(is_array($value)) {
                $value = json_encode($value);
            }

            $page->values()->updateOrCreate(['field_id' => $id],
            [
                'value' => $value,
                'field_id' => $id
            ]);
        }
    }
}
