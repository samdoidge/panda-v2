<?php

namespace Panda\Http\Controllers\Pages;

use Panda\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;

class ApiController extends Controller
{
    public function index()
    {
        $pages = Page::all()->nest();

        return response()
            ->json($pages, 200);
    }

    public function update(Request $request, Page $page)
    {
        $updated = $page->update($request->all());

        return response()->json([
            'error' => !$updated
        ]);
    }

    public function destroy(Page $page)
    {
        $deleted = $page->delete();

        return response()->json([
            'error' => !$deleted
        ]);
    }
}