<?php

namespace Panda\Http\Controllers\Pages;

use Panda\Models\Block;
use Panda\Repositories\PageRepository;
use Panda\Http\Controllers\BasePublicController;

class PublicController extends BasePublicController
{
    protected $page;
    protected $fields;

    public function __construct(PageRepository $repository)
    {
        parent::__construct($repository);

        // Get the page model based on the URL.
        $this->page = $repository->getFirstByUri($this->request->path());

        // Get the dynamic page fields and values.
        $this->fields = $this->getFields($this->page);
    }

    private function getFields($model)
    {
        $data = [];

        // Get all fields associated with the given template.
        $fields = $model->template->fields;

        // Get all values associated with the given model.
        $values = $model->values->lists('value', 'field_id');

        // Loop through the fields so we can assign values.
        foreach($fields as $field) {

            // If the value doesn't exist, set the value to null.
            $value = isset($values[$field->id]) ? $values[$field->id] : null;

            // Try to decode the value in-case it's JSON.
            $decode = json_decode($value);

            // Check to see if the decode worked, if not just reassign the value as it wasn't JSON.
            $value = $decode ? $decode : $value;

            // Check to see if the field is the block field.
            if($value && $field->field_type_id === 1) {

                $blocks = [];

                // We need to loop through the blocks to get the fields/values.
                foreach($value as $blockId) {

                    // Find the given block.
                    $block = Block::find($blockId);

                    if($block) {

                        // Block found, lets decode the fields and add it to the value.
                        $blocks[] = $this->getFields($block);
                    }
                }

                $value = $blocks;
            }

            $data[$field->name] = $value;
        }

        return $data;
    }
}