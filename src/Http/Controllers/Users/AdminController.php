<?php

namespace Panda\Http\Controllers\Users;

use Panda\Models\User;
use Panda\Repositories\UserRepository;
use Panda\Http\Requests\Users\FormRequest;
use Panda\Http\Controllers\BaseAdminController;

class AdminController extends BaseAdminController
{
    public function __construct(UserRepository $user)
    {
        parent::__construct($user);
    }

    public function create()
    {
        $model = $this->repository->getModel();

        return view('panda::pages.global.create')
            ->with(compact('model'));
    }

    public function edit(User $model)
    {
        return view('panda::pages.global.edit')
            ->with(compact($model));
    }
    
    public function store(FormRequest $request)
    {
        $model = $this->repository->create($request->all());

        return $this->redirect($request, $model);
    }
    
    public function update(User $user, FormRequest $request)
    {
        $this->repository->update($request->all());

        return $this->redirect($request, $user);
    }
}