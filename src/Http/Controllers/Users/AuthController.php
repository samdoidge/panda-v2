<?php

namespace Panda\Http\Controllers\Users;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function index()
    {
        return view('panda::pages.auth.index');
    }

    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->get('email'),
            'password' => $request->get('password')
        ];

        if(Auth::attempt($credentials)) {
            return redirect()
                ->route('admin.dashboard.index');
        }

        return redirect()
            ->route('admin.auth.index');
    }
}