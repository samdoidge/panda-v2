<?php

namespace Panda\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Crypt;
use JavaScript;
use Illuminate\Http\Request;

class JavaScriptData
{
    public function handle(Request $request, Closure $next)
    {
        JavaScript::put([
            '_token' => csrf_token(),
            'encrypted_token' => Crypt::encrypt(csrf_token())
        ]);

        return $next($request);
    }
}