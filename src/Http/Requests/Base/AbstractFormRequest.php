<?php

namespace Panda\Http\Requests\Base;

use Illuminate\Foundation\Http\FormRequest;

abstract class AbstractFormRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }
}