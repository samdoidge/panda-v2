<?php

namespace Panda\Http\Requests\Files;

use Panda\Http\Requests\Base\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        $rules = [
            'alt_attribute' => 'max:255'
        ];

        return $rules;
    }
}