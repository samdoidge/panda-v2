<?php

namespace Panda\Http\Requests\Groups;

use Panda\Http\Requests\Base\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        return [
            'name' => 'required|min:4|max:255|unique:groups,name,'.$this->id
        ];
    }
}