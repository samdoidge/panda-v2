<?php

namespace Panda\Http\Requests\Users;

use Panda\Http\Requests\Base\AbstractFormRequest;

class FormRequest extends AbstractFormRequest
{
    public function rules()
    {
        $rules = [
            'email' => 'required|email|max:255|unique:users,email,'.$this->id,
            'password' => 'required|min:8|max:255|confirmed'
        ];

        return $rules;
    }
}