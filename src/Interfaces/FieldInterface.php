<?php

namespace Panda\Interfaces;

use Panda\Models\Field;

interface FieldInterface
{
    public function __construct(Field $field, $value);
}