<?php

namespace Panda\Interfaces;

use Panda\Models\Page;

interface PageInterface extends RepositoryInterface
{
    public function getFirstByUri($uri, array $with = []);

    public function saveCache(Page $page);
}