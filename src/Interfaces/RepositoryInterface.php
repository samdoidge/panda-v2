<?php

namespace Panda\Interfaces;

interface RepositoryInterface
{
    public function getModel();

    public function getTable();

    public function make(array $with = []);

    public function getFirstBy($key, $value, array $with = [], $all = false);

    public function all(array $with = [], $all = false);

    public function allNested(array $with = [], $all = false);

    public function allBy($key, $value, array $with = [], $all = false);

    public function allBut($key, $value, array $with = [], $all = false);

    public function create(array $data);

    public function update(array $data);

    public function delete($model);
}