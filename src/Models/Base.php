<?php

namespace Panda\Models;

use Illuminate\Support\Facades\Log;
use InvalidArgumentException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Base extends Model
{
    public function scopeOnline(Builder $query)
    {
        return $query->where('status', 1);
    }

    public function scopeOrder(Builder $query)
    {
        return $query;
    }

    public function editUrl()
    {
        try {
            return route('admin.'.$this->getTable().'.edit', $this->id);
        } catch(InvalidArgumentException $e) {
            Log::error($e->getMessage());
        }
    }

    public function indexUrl()
    {
        try {
            return route('admin.'.$this->getTable().'.index');
        } catch(InvalidArgumentException $e) {
            Log::error($e->getMessage());
        }
    }
}