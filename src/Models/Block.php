<?php

namespace Panda\Models;

class Block extends Base
{
    protected $fillable = [
        'title',
        'template_id'
    ];

    public function template()
    {
        return $this->belongsTo('Panda\Models\Template');
    }

    public function values()
    {
        return $this->belongsToMany('Panda\Models\FieldValue');
    }
}