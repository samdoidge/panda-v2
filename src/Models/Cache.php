<?php

namespace Panda\Models;

class Cache extends Base
{
    protected $fillable = [
        'value'
    ];
}