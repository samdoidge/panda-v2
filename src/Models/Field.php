<?php

namespace Panda\Models;

class Field extends Base
{
    protected $fillable = [
        'name',
        'label',
        'order',
        'size',
        'validation',
        'field_type_id',
        'template_id'
    ];

    public function type()
    {
        return $this->belongsTo('Panda\Models\FieldType', 'field_type_id');
    }

    public function options()
    {
    	return $this->hasMany('Panda\Models\FieldOption');
    }
}