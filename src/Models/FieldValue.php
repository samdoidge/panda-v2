<?php

namespace Panda\Models;

class FieldValue extends Base
{
    protected $fillable = [
        'value',
        'field_id'
    ];
}