<?php

namespace Panda\Models;

class File extends Base
{
    protected $fillable = [
        'description',
        'alt_attribute',
        'type',
        'file',
        'path',
        'extension',
        'mimetype',
        'width',
        'height',
        'filesize',
        'position'
    ];

    public $attachments = [
        'file'
    ];
}