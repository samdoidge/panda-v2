<?php

namespace Panda\Models;

class Group extends Base
{
    protected $fillable = [
        'name'
    ];
}