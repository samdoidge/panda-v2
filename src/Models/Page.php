<?php

namespace Panda\Models;

use Panda\Traits\NestableTrait;

class Page extends Base
{
    use NestableTrait;
    
    protected $fillable = [
        'title',
        'slug',
        'uri',
        'parent_id',
        'template_id'
    ];

    public function values()
    {
        return $this->belongsToMany('Panda\Models\FieldValue');
    }

    public function parent()
    {
        return $this->belongsTo('Panda\Models\Page', 'parent_id');
    }

    public function template()
    {
        return $this->belongsTo('Panda\Models\Template');
    }

    public function cache()
    {
        return $this->morphOne('Panda\Models\Cache', 'cacheable');
    }

    public function previewUri()
    {
        if(!$this->id) {
            return '/';
        }

        $uri = $this->uri.'/'.$this->slug;

        return url($uri);
    }
}