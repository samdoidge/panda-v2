<?php

namespace Panda\Models;

class Template extends Base
{
    public function type()
    {
        return $this->hasOne('Panda\Models\TemplateType', 'id', 'template_type_id');
    }

    public function fields()
    {
        return $this->hasMany('Panda\Models\Field');
    }

    public function pages()
    {
    	return $this->hasMany('Panda\Models\Page');
    }

    public function blocks()
    {
        return $this->hasMany('Panda\Models\Block');
    }
}