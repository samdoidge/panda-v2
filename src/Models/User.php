<?php

namespace Panda\Models;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;

class User extends Base implements AuthenticatableContract
{
    use Authenticatable;

    protected $fillable = [
        'email',
        'password'
    ];
}