<?php

namespace Panda\Observers;

class CacheObserver
{
    public function saved($model)
    {
        $data = [];

        $values = $model->values->lists('value', 'field_id');

        foreach($model->template->fields as $field) {
            $data[$field->name] = '';
            if(isset($values[$field->id])) {
                $data[$field->name] = $values[$field->id];
            }
        }

        $data = json_encode($data);

        if($model->cache) {
            $model->cache()->update(['value' => $data]);
        } else {
            $model->cache()->create(['value' => $data]);
        }
    }
}