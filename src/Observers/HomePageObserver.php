<?php

namespace Panda\Observers;

use Panda\Models\Page;

class HomePageObserver
{
    public function saving(Page $model)
    {
        if($model->is_home) {
            $query = Page::where('is_home', 1);
            if($model->id) {
                $query->where('id', '!=', $model->id);
            }
            $query->update(['is_home' => 0]);
        }
    }
}