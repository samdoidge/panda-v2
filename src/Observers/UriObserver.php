<?php

namespace Panda\Observers;

use Panda\Models\Page;

class UriObserver
{
    public function creating(Page $model)
    {
        $model->uri = $this->incrementWhileExists($model, $model->slug);
    }

    public function updating(Page $model)
    {
        $parentUri = $this->getParentUri($model);

        if($parentUri) {
            $uri = $parentUri;
            if($model->slug) {
                $uri .= '/'.$model->slug;
            }
        } else {
            $uri = '/'.$model->slug;
        }

        $model->uri = $this->incrementWhileExists($model, $uri, $model->id);
    }

    private function getParentUri(Page $model)
    {
        if($parentPage = $model->parent) {
            return $parentPage->uri;
        }
    }

    private function incrementWhileExists(Page $model, $uri, $id = null)
    {
        if(!$uri) {
            return;
        }

        $originalUri = $uri;

        $i = 0;

        while($this->uriExists($model, $uri, $id)) {
            $i++;
            $uri = $originalUri.'-'.$i;
        }

        return $uri;
    }

    private function uriExists(Page $model, $uri, $id)
    {
        $query = $model->where('uri', $uri);

        if($id) {
            $query->where('id', '!=', $id);
        }

        if($query->first()) {
            return true;
        }

        return false;
    }
}