<?php

namespace Panda\Providers;

use Illuminate\Support\ServiceProvider;
use Panda\Commands\DatabaseCommand;
use Panda\Commands\InstallCommand;
use Panda\Events\ResetChildrenEvent;
use Panda\Models\File;
use Panda\Models\Page;
use Panda\Observers\CacheObserver;
use Panda\Observers\FileObserver;
use Panda\Observers\HomePageObserver;
use Panda\Observers\UriObserver;
use Panda\Services\FileUpload;

class PandaServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->loadViewsFrom(__DIR__.'/../resources/views/', 'panda');
        $this->loadTranslationsFrom(__DIR__.'/../resources/lang','panda');

        $this->commands('command.install');
        $this->commands('command.database');

        Page::observe(new HomePageObserver());
        Page::observe(new UriObserver());
        //Page::observe(new CacheObserver());
    }

    public function register()
    {
        $app = $this->app;

        $this->registerProviders();
        $this->registerAliases();
        $this->registerCommands();

        $app->singleton('upload.file', function() {
            return new FileUpload();
        });

        $app->events->subscribe(new ResetChildrenEvent());
    }

    private function registerProviders()
    {
        $app = $this->app;

        $app->register('Panda\Providers\RouteServiceProvider');
        $app->register('Barryvdh\Elfinder\ElfinderServiceProvider');
        $app->register('Krucas\Notification\NotificationServiceProvider');
        $app->register('Laracasts\Utilities\JavaScript\JavaScriptServiceProvider');
    }

    private function registerAliases()
    {
        $app = $this->app;

        $app->alias('Notification', 'Krucas\Notification\Facades\Notification');
        $app->alias('Pages', 'Panda\Facades\PageFacade');
    }

    private function registerCommands()
    {
        $app = $this->app;

        $app->bind('command.install', function() {
            return new InstallCommand();
        });

        $app->bind('command.database', function() {
            return new DatabaseCommand();
        });
    }
}