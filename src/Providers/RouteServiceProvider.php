<?php

namespace Panda\Providers;

use Pages;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Routing\Router;

class RouteServiceProvider extends ServiceProvider
{
    protected $namespace = 'Panda\Http\Controllers';

    public function boot(Router $router)
    {
        parent::boot($router);

        $router->bind('uri', function($uri) {

            if($uri === '/') {
                return Pages::getFirstBy('is_home', 1, []);
            }

            return Pages::getFirstByUri($uri, []);
        });
    }
    
    public function map(Router $router)
    {
        $router->group(['namespace' => $this->namespace], function(Router $router) {

            /**
             * AuthController
             */
            $router->get('admin', ['as' => 'admin.auth.index', 'uses' => 'Users\AuthController@index']);
            $router->post('admin', ['as' => 'admin.auth.login', 'uses' => 'Users\AuthController@login']);

            /**
             * DashboardController
             */
            $router->get('admin/dashboard', ['as' => 'admin.dashboard.index', 'uses' => 'Dashboard\AdminController@index']);

            /**
             * PageController
             */
            $router->get('admin/pages', ['as' => 'admin.pages.index', 'uses' => 'Pages\AdminController@index']);
            $router->get('admin/pages/create', ['as' => 'admin.pages.create', 'uses' => 'Pages\AdminController@create']);
            $router->post('admin/pages', ['as' => 'admin.pages.store', 'uses' => 'Pages\AdminController@store']);
            $router->get('admin/pages/{page}/edit', ['as' => 'admin.pages.edit', 'uses' => 'Pages\AdminController@edit']);
            $router->put('admin/pages/{page}', ['as' => 'admin.pages.update', 'uses' => 'Pages\AdminController@update']);

            $router->get('api/pages', ['as' => 'api.pages.index', 'uses' => 'Pages\ApiController@index']);
            $router->put('api/pages/{page}', ['as' => 'api.pages.update', 'uses' => 'Pages\ApiController@update']);
            $router->delete('api/pages/{page}', ['as' => 'api.pages.destroy', 'uses' => 'Pages\ApiController@destroy']);

            /**
             * FileController
             */
            $router->get('admin/files', ['as' => 'admin.files.index', 'uses' => 'Files\AdminController@index']);

            $router->get('api/files', ['as' => 'api.files.index', 'uses' => 'Files\ApiController@index']);
            $router->post('api/files', ['as' => 'api.files.store', 'uses' => 'Files\ApiController@store']);
            $router->delete('api/files/{file}', ['as' => 'api.files.destroy', 'uses' => 'Files\ApiController@destroy']);

            /**
             * UserController
             */
            $router->get('admin/users', ['as' => 'admin.users.index', 'uses' => 'Users\AdminController@index']);
            $router->get('admin/users/create', ['as' => 'admin.users.create', 'uses' => 'Users\AdminController@create']);
            $router->post('admin/users', ['as' => 'admin.users.store', 'uses' => 'Users\AdminController@store']);
            $router->get('admin/users/{user}/edit', ['as' => 'admin.users.edit', 'uses' => 'Users\AdminController@edit']);
            $router->put('admin/users/{user}', ['as' => 'admin.users.update', 'uses' => 'Users\AdminController@update']);

            /**
             * GroupController
             */
            $router->get('admin/groups', ['as' => 'admin.groups.index', 'uses' => 'Groups\AdminController@index']);
            $router->get('admin/groups/create', ['as' => 'admin.groups.create', 'uses' => 'Groups\AdminController@create']);
            $router->post('admin/groups', ['as' => 'admin.groups.store', 'uses' => 'Groups\AdminController@store']);
            $router->get('admin/groups/{group}/edit', ['as' => 'admin.groups.edit', 'uses' => 'Groups\AdminController@edit']);
            $router->put('admin/groups/{group}', ['as' => 'admin.groups.update', 'uses' => 'Groups\AdminController@update']);

            /**
             * BlockController
             */
            $router->get('admin/blocks', ['as' => 'admin.blocks.index', 'uses' => 'Blocks\AdminController@index']);
            $router->get('admin/blocks/create', ['as' => 'admin.blocks.create', 'uses' => 'Blocks\AdminController@create']);
            $router->post('admin/blocks', ['as' => 'admin.blocks.store', 'uses' => 'Blocks\AdminController@store']);
            $router->get('admin/blocks/{model}/edit', ['as' => 'admin.blocks.edit', 'uses' => 'Blocks\AdminController@edit']);
            $router->put('admin/blocks/{model}', ['as' => 'admin.blocks.update', 'uses' => 'Blocks\AdminController@update']);

            /**
             * FieldController
             */
            $router->get('api/fields/{template}/{page?}', ['as' => 'api.fields.index', 'uses' => 'Fields\ApiController@index']);

            /**
             * TemplateController
             */
            $router->get('api/templates/{model}', ['as' => 'api.templates.index', 'uses' => 'Templates\ApiController@index']);

            /**
             * PublicController
             */
            $router->get('{uri}', 'Pages\PublicController@index')->where('uri', '(.*)');
        });
    }
}