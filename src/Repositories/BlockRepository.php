<?php

namespace Panda\Repositories;

use Panda\Models\Block;
use Panda\Interfaces\RepositoryInterface;
use Panda\Repositories\RepositoryAbstract;

class BlockRepository extends RepositoryAbstract implements RepositoryInterface
{
	public function __construct(Block $model)
	{
		$this->model = $model;
	}
}