<?php

namespace Panda\Repositories;

use Panda\Interfaces\RepositoryInterface;
use Panda\Models\File;

class FileRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function __construct(File $model)
    {
        $this->model = $model;
    }
}