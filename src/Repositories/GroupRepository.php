<?php

namespace Panda\Repositories;

use Panda\Interfaces\RepositoryInterface;
use Panda\Models\Group;

class GroupRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function __construct(Group $model)
    {
        $this->model = $model;
    }
}