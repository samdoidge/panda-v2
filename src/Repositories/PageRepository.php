<?php

namespace Panda\Repositories;

use Illuminate\Support\Facades\Request;
use Panda\Interfaces\PageInterface;
use Panda\Interfaces\RepositoryInterface;
use Panda\Models\Page;

class PageRepository extends RepositoryAbstract implements PageInterface
{
    public function __construct(Page $model)
    {
        $this->model = $model;
    }

    public function update(array $data)
    {
        $model = $this->model->find($data['id']);

        $model->fill($data);

        if($model->save()) {
            event('resetChildrenUri', [$model]);
            return true;
        }

        return false;
    }

    public function getFirstByUri($uri, array $with = [])
    {
        $model = $this->make($with)
            ->where('uri', $uri)
            ->where(function($query) {
                if(!Request::input('preview')) {
                    $query->where('status', 1);
                }
            })
            ->firstOrFail();

        return $model;
    }

    public function saveCache(Page $page)
    {
        $data = [];
        $values = $page->values->lists('value', 'field_id');

        foreach($page->template->fields as $field) {
            $data[$field->name] = '';
            if(isset($values[$field->id])) {
                $data[$field->name] = $values[$field->id];
            }
        }

        $data = json_encode($data);
        $page->cache()->updateOrCreate(['cacheable_id' => $page->id], ['value' => $data]);
    }
}