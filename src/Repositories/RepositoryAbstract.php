<?php

namespace Panda\Repositories;

use Panda\Interfaces\RepositoryInterface;

class RepositoryAbstract implements RepositoryInterface
{
    protected $model;

    public function getModel()
    {
        return $this->model;
    }

    public function getTable()
    {
        return $this->model->getTable();
    }

    public function make(array $with = [])
    {
        return $this->model->with($with);
    }

    public function getFirstBy($key, $value, array $with = [], $all = false)
    {
        $query = $this->make($with);

        if(!$all) {
            $query->online();
        }

        return $query->where($key, $value)->first();
    }

    public function all(array $with = [], $all = false)
    {
        $query = $this->make($with);

        if(!$all) {
            $query->online();
        }

        $query->order();

        return $query->get();
    }

    public function allNested(array $with = [], $all = false)
    {
        return $this->all($with, $all)->nest();
    }

    public function allBy($key, $value, array $with = [], $all = false)
    {
        $query = $this->make($with);

        if(!$all) {
            $query->online();
        }

        $query->where($key, $value);

        $query->order();

        return $query->get();
    }

    public function allBut($key, $value, array $with = [], $all = false)
    {
        $query = $this->make($with);

        if(!$all) {
            $query->online();
        }

        $query->where($key, '!=', $value);

        $query->order();

        return $query->get();
    }

    public function create(array $data)
    {
        $model = $this->model->fill($data);

        if($model->save()) {
            return $model;
        }

        return false;
    }

    public function update(array $data)
    {
        $model = $this->model->find($data['id']);

        $model->fill($data);

        if($model->save()) {
            return true;
        }

        return false;
    }

    public function delete($model)
    {
        return $model->delete();
    }
}