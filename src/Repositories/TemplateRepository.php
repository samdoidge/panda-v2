<?php

namespace Panda\Repositories;

use Panda\Models\Template;
use Panda\Interfaces\RepositoryInterface;

class TemplateRepository extends RepositoryAbstract implements RepositoryInterface
{
	public function __construct(Template $model)
	{
		$this->model = $model;
	}
}