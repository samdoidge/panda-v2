<?php

namespace Panda\Repositories;

use Panda\Interfaces\RepositoryInterface;
use Panda\Models\User;

class UserRepository extends RepositoryAbstract implements RepositoryInterface
{
    public function __construct(User $model)
    {
        $this->model = $model;
    }

    public function create(array $data)
    {
        $model = $this->model;

        $userData = array_except($data, ['_method', '_token', 'id', 'exit', 'password_confirmation']);
        $userData['password'] = bcrypt($data['password']);

        foreach($userData as $key => $value) {
            $model->$key = $value;
        }

        if($model->save()) {
            return $model;
        }

        return false;
    }

    public function update(array $data)
    {
        $user = $this->model->find($data['id']);

        $userData = array_except($data, ['_method', '_token', 'exit', 'password_confirmation']);

        if(!$userData['password']) {
            $userData = array_except($userData, 'password');
        } else {
            $user['password'] = bcrypt($data['password']);
        }

        foreach($userData as $key => $value) {
            $user->$key = $value;
        }

        if($user->save()) {
            return true;
        }

        return false;
    }
}