<?php

namespace Panda\Services;

use Notification;
use Illuminate\Support\Str;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class FileUpload
{
    public function handle(UploadedFile $file, $path = 'uploads')
    {
        $input = [];

        $fileName = Str::slug(pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME));
        $fileName = preg_replace('#([0-9_]+)x([0-9_]+)#', '$1-$2', $fileName);

        $input['path'] = $path;
        $input['extension'] = '.'.$file->getClientOriginalExtension();
        $input['filesize'] = $file->getClientSize();
        $input['mimetype'] = $file->getClientMimeType();
        $input['filename'] = $fileName.$input['extension'];
        $input['type'] = 'o';

        $fileCounter = 1;
        while(file_exists($input['path'].'/'.$input['filename'])) {
            $input['filename'] = $fileName.'_'.$fileCounter++.$input['extension'];
        }
        
        try {
            $file->move($input['path'], $input['filename']);
            list($input['width'], $input['height']) = getimagesize($input['path'].'/'.$input['filename']);
            return $input;
        } catch(FileException $e) {
            Notification::error($e->getMessage());
            return false;
        }
    }
}