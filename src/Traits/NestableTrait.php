<?php

namespace Panda\Traits;

use Panda\Helpers\NestableHelper;

trait NestableTrait
{
    public function newCollection(array $models = array())
    {
        return new NestableHelper($models);
    }
}