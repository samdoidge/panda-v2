<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pages', function(Blueprint $table) {
            $table->increments('id');
            $table->string('slug')->nullable();
            $table->string('uri')->nullable();
            $table->string('title');
            $table->boolean('status')->default(0);
            $table->integer('position')->unsigned()->default(0);
            $table->boolean('is_home')->default(0);
            $table->integer('template_id')->unsigned();
            $table->integer('parent_id')->unsigned()->nullable()->default(null);
            $table->string('meta_title')->nullable();
            $table->text('meta_description')->nullable();
            $table->text('meta_keywords')->nullable();
            $table->timestamps();
            $table->foreign('template_id')->references('id')->on('templates')->onDelete('cascade');
            $table->foreign('parent_id')->references('id')->on('pages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('pages');
    }
}
