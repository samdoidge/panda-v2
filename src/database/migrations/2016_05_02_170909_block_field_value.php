<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class BlockFieldValue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('block_field_value', function(Blueprint $table) {
            $table->integer('field_value_id')->unsigned();
            $table->integer('block_id')->unsigned();
            $table->foreign('field_value_id')->references('id')->on('field_values')->onDelete('cascade');
            $table->foreign('block_id')->references('id')->on('blocks')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('block_field_value');
    }
}
