(function(angular) {
    'use strict';
    angular.module('panda', ['smart-table', 'ui.tree']);
}(angular));