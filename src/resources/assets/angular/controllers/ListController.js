(function(angular) {
    'use strict';
    angular.module('panda').controller('ListController', ['$http', '$scope', '$location', function ($http, $scope, $location) {
        var url = $location.absUrl().split('?')[0],
            moduleName = url.split('/')[4];

        $scope.PandaCMS = PandaCMS;

        if(PandaCMS.models) {
            $scope.models = PandaCMS.models;
            $scope.displayedModels = [].concat($scope.models);
        } else {
            $http.get('/api/' + moduleName).then(function (all) {
                $scope.models = all.data;
                $scope.displayedModels = [].concat($scope.models);
            });
        }

        $scope.toggleStatus = function(model) {
            var newStatus = Math.abs(model.status - 1),
                label = (newStatus === 1)
                    ? 'online'
                    : 'offline';
            model.status = newStatus;
            $http.put('/api/' + moduleName + '/' + model.id, model).then(function () {
                alertify.success('Item is ' + label + '.');
            }, function (reason) {
                alertify.error('Error ' + reason.status + ' ' + reason.statusText);
            });
        };

        $scope.delete = function(model, title) {
             if(!title) {
                title = model.title;
             }
             if(!window.confirm('Remove ' + title + '?')) {
                return false;
             }
             var index = $scope.models.indexOf(model);
             $http.delete('/api/' + moduleName + '/' + scope.model.id).then(function(data) {
                if(index !== -1) {
                    $scope.models.splice(index, 1);
                }
                if(data.error) {
                    alertify.error('Error');
                }
             }, function(reason) {
                alertify.error('Error ' + reason.status + ' ' + reason.statusText);
             });
        };

        $scope.deleteFromNested = function(scope, title) {
            if(!title) {
                title = scope.model.title;
            }
            if(scope.hasChild()) {
                alertify.error('Cannot delete item because it has children.');
            }
            if (!window.confirm('Remove ' + title + '?')) {
                return false;
            }
            $http.delete('/api/' + moduleName + '/' + scope.model.id).then(function (data) {
                scope.remove();
                if (data.error) {
                    alertify.error('Error');
                }
            }, function (reason) {
                alertify.error('Error ' + reason.status + ' ' + reason.statusText);
            });
        };
    }]);
}(angular));