(function(angular) {
    'use strict';
    angular.module('panda').controller('PageController', ['$http', '$scope', '$sce', '$location', function ($http, $scope, $sce, $location) {
        $scope.templateSelected = null;
        $scope.templates = {};
        $scope.fields = '';

        var url = $location.absUrl().split('?')[0],
            module = url.split('/')[4],
            param = url.split('/')[5],
            pageId = '',
            templateType = 1;

        if(module === 'blocks') {
            templateType = 2;
        }

        if (param.length > 0 && param % 1 === 0) {
            pageId = '/' + param;
        }

        $http.get('/api/templates/' + templateType).then(function (response) {
            $scope.templates = response.data;
            if ($scope.templates.length > 0) {
                $scope.templateSelected = $scope.templates[0];
                $scope.getFields($scope.templateSelected.id);
            }
        });

        $scope.getFields = function (templateId) {
            $http.get('/api/fields/' + templateId + pageId).then(function (response) {
                $scope.fields = $sce.trustAsHtml(response.data);
            });
        }
    }]);
}(angular));