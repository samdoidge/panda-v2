angular.module('panda').directive('dropZone', function() {
    return function(scope, element, attrs) {
        $('#uploaderAddButtonContainer').on('click', function() {
            return false;
        });
        $('#uploaderAddButton').on('click', function() {
            $('#dropzone').trigger('click');
        });
        Dropzone.options.dropzone = {
            url: '/api/files',
            paramName: 'file',
            clickable: true,
            maxFilesize: 60,
            thumbnailWidth: 140,
            thumbnailHeight: 140,
            init: function() {
                this.on('success', function(file, response) {
                    var $this = this;
                    window.setTimeout(function() {
                        $(file.previewElement).fadeOut('fast', function() {
                            $this.removeFile(file);
                            scope.$apply(function() {
                                scope.models.splice(0, 0, response.model);
                            });
                        });
                    }, 1000);
                });
                this.on('error', function(file, response) {
                    var message = null;
                    if($.type(response) === 'string') {
                        message = response;
                    }  else {
                        message = response.file[0];
                    }
                    file.previewElement.classList.add('dz-error');
                    _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]');
                    _results = [];
                    for(_i = 0, _len = _ref.length; _i < _len; _i++) {
                        node = _ref[_i];
                        _results.push(node.textContent = message);
                    }
                    return _results;
                });
                this.on('sending', function(file, xhr, formData) {
                    formData.append('_token', PandaCMS._token);
                    formData.append('description', '');
                    formData.append('alt_attribute', '');
                    formData.append('keywords', '');
                    formData.append('status', 1);
                });
            }
        };
    }
});