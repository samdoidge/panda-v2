angular.module('panda').directive('pandaBtnDelete', function()
{
    return {
        scope: {
            action: '&'
        },
        template:
        '<div ng-click="action()" class="btn btn-xs btn-link">' +
        '<span class="fa fa-remove"></span>' +
        '<span class="sr-only">Delete</span>' +
        '</div>'
    };
});
