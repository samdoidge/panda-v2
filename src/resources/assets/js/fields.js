$(function() {
    var $element = $('#template_id'),
        $container = $('#fields'),
        $id = $('#id'),
        $modelId = '';
    if($element.length > 0 && $container.length > 0) {
        $element.on('change', function() {
            getFields($(this).val());
        });
        if($id.length > 0 && $id.val().length > 0) {
            $modelId = '/' + $id.val();
        }
        function getFields(templateId) {
            $.get('/api/fields/' + templateId + $modelId, function(data) {
                $container.html(data);
            });
        }
        getFields($element.val());
    }
});