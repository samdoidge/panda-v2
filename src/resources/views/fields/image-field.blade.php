<div class="col-md-{{ $field->size }}">
    <div class="image-field" data-id="fields[{{ $field->id }}]">
        <div class="form-group">
            <label class="control-label" for="fields[{{ $field->id }}]">{{ $field->label }}</label>
            <div class="well">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="images">
                                @foreach($images as $image)
                                    <div class="image">
                                        <div class="col-md-2">
                                            <img src="{{ $image->url }}" class="img-responsive img-rounded">
                                        </div>
                                    </div>
                                @endforeach
                            </div>
                        </div>
                    </div>
                    <div class="controls @if(!$images){{ 'hidden' }}@endif">
                        <div class="col-md-4">
                            <div class="well">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label class="control-label" for="alt">Filename</label>
                                            <input type="text" id="filename" name="filename" class="form-control" value="{{ isset($images[0]) && $images[0]->filename }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="alt">Alternative Attribute</label>
                                            <input type="text" id="alt" name="alt" class="form-control" value="{{ isset($images[0]) && $images[0]->alt }}">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label" for="fields[{{ $field->id }}]">Data</label>
                                            <input type="text" id="fields[{{ $field->id }}]" name="fields[{{ $field->id }}]" value="{{ $value }}" class="form-control">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <a href="" class="btn btn-default btn-elfinder">Add New Image</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $('.btn-elfinder').on('click', function(e) {
        e.preventDefault();
        $('<div id="editor" />').dialogelfinder({
            url : '{{ route('elfinder.connector') }}',
            getFileCallback: function(file) {
                $parent = $(e.target).parents('.image-field');
                $imagesElement = $parent.find('.images');
                $editor = $('#editor');
                $editor.dialogelfinder('close');
                $editor.closest('.elfinder').val(file.path);
                $editor.remove();
                $imagesElement.append('<div class="image"><div class="col-md-2"><img src="' + file.url + '" class="img-responsive img-rounded"></div></div>');
                appendImage(file, $parent);
            }
        });
    });
    function control(file, parent) {
        parent.find('.controls').show();
        parent.find('#filename').val(file.filename);
        parent.find('#alt').val(file.alt);
    }
    function appendImage(file, parent) {
        $element = $parent.find('input[name="' + parent.data('id') + '"]');
        $images = [];
        if($element.val() > 0) {
            $images = JSON.parse($element.val());
        }
        $image = {};
        $image.url = '/' + file.path;
        $image.filename = file.name;
        $image.alt = '';
        $images.push($image);
        $json = JSON.stringify($images);
        $element.val($json);
        control($image);
    }
</script>