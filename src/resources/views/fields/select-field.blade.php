<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="fields[{{ $field->id }}]@if($multiple){{ '[]' }}@endif">{{ $field->label }}</label>
        <select name="fields[{{ $field->id}}]@if($multiple){{ '[]' }}@endif" class="form-control" id="fields[{{ $field->id }}]@if($multiple){{ '[]' }}@endif" @if($multiple){{ 'multiple' }}@endif>
            @foreach($data as $key => $option)
                <option value="{{ $key }}" @if($key == $value || (is_array($value) && in_array($key, $value))){{ 'selected' }}@endif>{{ $option }}</option>
            @endforeach
        </select>
    </div>
</div>