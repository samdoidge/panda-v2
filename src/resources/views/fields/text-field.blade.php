<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="fields[{{ $field->id }}]">{{ $field->label }}</label>
        <input type="text" name="fields[{{ $field->id }}]" class="form-control" id="fields[{{ $field->id }}]" value="{{ $value }}">
    </div>
</div>