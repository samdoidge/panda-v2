<div class="col-md-{{ $field->size }}">
    <div class="form-group">
        <label class="control-label" for="fields[{{ $field->id }}]">{{ $field->label }}</label>
        <textarea name="fields[{{ $field->id }}]" class="form-control ckeditor" id="fields[{{ $field->id }}]">{{ $value }}</textarea>
    </div>
</div>
<script type="text/javascript">
    CKEDITOR.replace('fields[{{ $field->id }}]');
</script>