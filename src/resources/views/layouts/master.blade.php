<!DOCTYPE html>

<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>-</title>
        <link rel="stylesheet" href="{{ asset('css/panda.css') }}">
        @yield('css')
        <link rel="stylesheet" href="{{ asset('components/jquery-ui/jquery-ui.min.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/barryvdh/elfinder/css/elfinder.min.css') }}">
        <link rel="stylesheet" href="{{ asset('packages/barryvdh/elfinder/css/theme.css') }}">
    </head>
    <body class="@yield('class')">
        @yield('scaffolding')
        @include('panda::partials.layouts.javascript')
        <script src="{{ asset('js/panda.min.js') }}"></script>
        @yield('js')
        <script src="{{ asset('components/jquery-ui/jquery-ui.min.js') }}"></script>
        <script src="{{ asset('packages/barryvdh/elfinder/js/elfinder.min.js') }}"></script>
        <script type="text/javascript">
            $(function() {
                $('#elfinder').elfinder({
                    url: '{{ route('elfinder.connector') }}'
                });
            });
        </script>
        <script type="type/javascript">
            {!! Notification::showError('alertify.error(\':message\');') !!}
            {!! Notification::showInfo('alertify.log(\':message\');') !!}
            {!! Notification::showSuccess('alertify.success(\':message\');') !!}
        </script>
    </body>
</html>