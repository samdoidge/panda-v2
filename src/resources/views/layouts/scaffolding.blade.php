@extends('panda::layouts.master')
@section('class', 'has-navbar')
@section('scaffolding')
    @include('panda::partials.layouts.navbar')
    <div class="container-fluid">
        <div class="row">
            @include('panda::partials.layouts.sidebar')
            <div class="col-xs-12 col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                @section('errors')
                    @if(!$errors->isEmpty())
                        <div class="alert alert-danger alert-dismissable">
                            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                            <span>The form contains errors.</span>
                            <ul>
                                @foreach($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                @show
                @yield('body')
            </div>
        </div>
    </div>
@endsection