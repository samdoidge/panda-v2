@extends('panda::layouts.master')

@section('class', 'auth')

@section('scaffolding')
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <div class="text-center">
                    <div class="form-group">
                        <img src="http://placehold.it/120x120" width="120" height="120" alt="-" class="brand img-circle">
                        <h1>SCAN DESIGN</h1>
                    </div>
                </div>
            </div>
            <div class="col-sm-4 col-sm-offset-4">
                <form action="{{ route('admin.auth.login') }}" method="post" class="text-center">
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" id="email" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="password" name="password" class="form-control" id="password" placeholder="Password">
                    </div>
                    <button type="submit" class="btn btn-primary">SIGN IN</button>
                    <a href="#">Forgotten your password?</a>
                </form>
            </div>
        </div>
    </div>
@endsection