@extends('panda::layouts.scaffolding')

@section('body')
    <h1>
        @lang('panda::'.$model->getTable().'.new')
    </h1>
    <form action="{{ route('admin.'.$model->getTable().'.store') }}" method="POST" role="form">
        {{ csrf_field() }}
        @include('panda::pages.'.$model->getTable().'.form')
    </form>
@endsection