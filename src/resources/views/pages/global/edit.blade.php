@extends('panda::layouts.scaffolding')

@section('body')
    <h1>
        @lang('panda::'.$model->getTable().'.edit')
    </h1>
    <form action="{{ route('admin.'.$model->getTable().'.update', $model->id) }}" method="POST">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        @include('panda::pages.'.$model->getTable().'.form')
    </form>
@endsection