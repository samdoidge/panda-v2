@include('panda::partials.form.actions')

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label" for="name">Name</label>
            <input type="text" name="name" id="name" class="form-control" autocomplete="off" value="{{ $model->name }}">
        </div>
    </div>
</div>