@section('js')
    <script src="{{ asset('components/ckeditor/ckeditor.js') }}"></script>
@endsection

@include('panda::partials.form.actions')
<input type="hidden" id="id" name="id" value="{{ $model->id }}">
<ul class="nav nav-tabs">
    <li class="active">
        <a href="#tab-content" data-target="#tab-content" data-toggle="tab">Content</a>
    </li>
    <li>
        <a href="#tab-meta" data-target="#tab-meta" data-toggle="tab">Meta</a>
    </li>
    <li>
        <a hrf="#tab-options" data-target="#tab-options" data-toggle="tab">Options</a>
    </li>
</ul>
<div class="tab-content">
    <div class="tab-pane active" id="tab-content">
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="title">Title</label>
                    <input type="text" name="title" class="form-control" id="title" value="{{ $model->title }}">
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="template_id">Template</label>
                    <select name="template_id" class="form-control" id="template_id">
                        @foreach($templates as $id => $name)
                            <option value="{{ $id }}" @if($id == $model->template_id){{ 'selected' }}@endif>{{ $name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="parent_id">Parent</label>
                    <select name="parent_id" class="form-control" id="parent_id">
                        <option value="0">-- Select --</option>
                        @foreach($parents as $pageId => $page)
                            <option value="{{ $pageId }}" @if($model->parent_id === $pageId){{ 'selected' }}@endif>{{ $page->title }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="control-label" for="slug">URL</label>
                    <div class="input-group">
                        <span class="input-group-addon">/</span>
                        <input type="text" name="slug" class="form-control" id="slug" value="{{ $model->slug }}" data-slug="title">
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="checkbox form-group">
                    <label class="control-label">
                        <input type="checkbox" name="status" id="status" value="1" {{ $model->status ? 'checked' : null }})>
                        <span>Online</span>
                    </label>
                </div>
            </div>
            <div id="fields"></div>
        </div>
    </div>
    <div class="tab-pane" id="tab-meta">
        <div class="row">
            <div class="col-md-12">
                <div class="form-group">
                    <label class="control-label" for="meta_title">Meta Title</label>
                    <input type="text" name="meta_title" class="form-control" id="meta_title" value="{{ $model->meta_title }}">
                </div>
                <div class="form-group">
                    <label class="control-label" for="meta_description">Meta Description</label>
                    <textarea name="meta_description" class="form-control" id="meta_description">{{ $model->meta_description }}</textarea>
                </div>
                <div class="form-group">
                    <label class="control-label" for="meta_keywords">Meta Keywords</label>
                    <textarea name="meta_keywords" class="form-control" id="meta_keywords">{{ $model->meta_keywords }}</textarea>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-pane" id="tab-options">
        <div class="row">
            <div class="col-md-12">
                <div class="checkbox form-group">
                    <label class="control-label">
                        <input type="checkbox" name="is_home" id="is_home" value="1" {{ $model->is_home ? 'checked' : null }}>
                        <span>Is home</span>
                    </label>
                </div>
            </div>
        </div>
    </div>
</div>