@extends('panda::layouts.scaffolding')

@section('body')
    <div ng-app="panda" ng-controller="ListController" ng-cloak>
        <h1>Pages</h1>
        <div class="btn-toolbar pull-left">
            <a href="{{ route('admin.pages.create') }}" class="btn btn-default">Add New</a>
        </div>
        <nav class="pull-right">
            <ul class="pagination">
                <li>
                    <a href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                    </a>
                </li>
                <li><a href="#">1</a></li>
                <li>
                    <a href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                    </a>
                </li>
            </ul>
        </nav>
        <div class="clearfix"></div>
        <div class="panel panel-default">
            <div class="table-responsive">
                <table class="table table-striped">
                    <thead class="thead-inverse">
                        <tr>
                            <th width="60%">Title</th>
                            <th width="20%">Author</th>
                            <th width="20%">Date</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($models as $model)
                            <tr>
                                <td>
                                    <div>
                                        <a href="{{ route('admin.pages.edit', $model->id) }}">{{ $model->title }}</a>
                                    </div>
                                    <div class="actions"><a href="{{ route('admin.pages.edit', $model->id) }}">Edit</a> | Delete | <a href="{{ url($model->uri) }}" target="_blank">View</a></div>
                                </td>
                                <td>admin</td>
                                <td>
                                    <div>
                                        <span>Published</span>
                                    </div>
                                    <div>
                                        <span>{{ $model->updated_at->format('d-m-Y') }}</span>
                                    </div>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection