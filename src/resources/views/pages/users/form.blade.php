@include('panda::partials.form.actions')

<div class="row">
    <div class="col-sm-12">
        <div class="form-group">
            <label class="control-label" for="email">Email</label>
            <input type="email" name="email" id="email" class="form-control" autocomplete="off" value="{{ $model->email }}">
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="password">Password</label>
            <input type="password" name="password" id="password" class="form-control">
        </div>
    </div>
    <div class="col-sm-6">
        <div class="form-group">
            <label class="control-label" for="password_confirmation">Password confirmation</label>
            <input type="password" name="password_confirmation" id="password_confirmation" class="form-control">
        </div>
    </div>
</div>