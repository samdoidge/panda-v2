@extends('panda::layouts.scaffolding')

@section('body')
    <div ng-app="panda" ng-controller="ListController" ng-cloak>
        <a href="{{ route('admin.users.create') }}" class="btn-add"><i class="fa fa-plus-circle"></i><span class="sr-only">New</span></a>
        <h1><span>Users</span></h1>
        <div class="btn-toolbar">
            @include('panda::partials.layouts.lang-switcher')
        </div>
        <div class="table-responsive">
            <table st-persist="usersTable" st-table="displayedModels" st-safe-src="models" st-order st-filter class="table table-condensed table-main">
                <thead>
                    <tr>
                        <th class="delete"></th>
                        <th class="edit"></th>
                        <th st-sort="email" class="email st-sort">Email</th>
                    </tr>
                    <tr>
                        <td colspan="2"></td>
                        <td>
                            <input st-search class="form-control input-sm" placeholder="Search..." type="text">
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr ng-repeat="model in displayedModels">
                        <td panda-btn-delete action="delete(model, model.email)"></td>
                        <td>
                            @include('panda::partials.form.edit')
                        </td>
                        <td>@{{ model.email }}</td>
                    </tr>
                </tbody>
                <tfoot>
                    <tr>
                        <td colspan="3" panda-pagination></td>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
@endsection