<div class="btn-toolbar">
    <button type="submit" value="true" id="edit" name="exit" class="btn btn-primary">Save and exit</button>
    <button type="submit" class="btn btn-default">Save</button>
    @if(method_exists($model, 'previewUri') && $previewUri = $model->previewUri())
        <a class="btn btn-default btn-preview" href="{{ $previewUri }}?preview=true">Preview</a>
    @endif
</div>