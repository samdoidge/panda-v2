<div class="col-xs-6 col-sm-3 col-md-2 sidebar" role="navigation">
    <ul class="sidebar-menu">
        <li class="sidebar-panel">
            <ul class="nav nav-sidebar panel-collapse collapse in" id="dashboard">
                <li class="clearfix {{ request()->is('admin/dashboard*') ? 'active' : '' }}">
                    <a href="{{ route('admin.dashboard.index') }}">
                        <i class="icon fa fa-fw fa-dashboard"></i>
                        <div>Dashboard</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Content</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="content">
                <li class="clearfix {{ request()->is('admin/pages*') ? 'active' : '' }}">
                    <a href="{{ route('admin.pages.index') }}">
                        <i class="icon fa fa-fw fa-file"></i>
                        <div>Pages</div>
                    </a>
                </li>
                <li class="clearfix {{ request()->is('admin/blocks*') ? 'active' : '' }}">
                    <a href="{{ route('admin.blocks.index') }}">
                        <i class="icon fa fa-fw fa-list-alt"></i>
                        <div>Blocks</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Media</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="media">
                <li class="clearfix {{ request()->is('admin/files*') ? 'active': '' }}">
                    <a href="{{ route('admin.files.index') }}">
                        <i class="icon fa fa-fw fa-file-photo-o"></i>
                        <div>Files</div>
                    </a>
                </li>
            </ul>
        </li>
        <li class="sidebar-panel">
            <div class="sidebar-title">Users and groups</div>
            <ul class="nav nav-sidebar panel-collapse collapse in" id="users">
                <li class="clearfix {{ request()->is('admin/users*') ? 'active': '' }}">
                    <a href="{{ route('admin.users.index') }}">
                        <i class="icon fa fa-fw fa-user"></i>
                        <div>Users</div>
                    </a>
                </li>
                <li class="clearfix {{ request()->is('admin/groups*') ? 'active': '' }}">
                    <a href="{{ route('admin.groups.index') }}">
                        <i class="icon fa fa-fw fa-users"></i>
                        <div>Groups</div>
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>